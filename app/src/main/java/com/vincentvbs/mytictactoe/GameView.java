package com.vincentvbs.mytictactoe;



interface GameView {
    void showInfo(String s);

    void placeCircle(int buttonIndex);

    void placeCross(int buttonIndex);

    void clearSquare(int buttonIndex);

    //void cleanBoard();
}
