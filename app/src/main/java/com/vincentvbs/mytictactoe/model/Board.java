package com.vincentvbs.mytictactoe.model;


public class Board {

    private Square mBoard[][];
    private final int ROW_SIZE = 3;
    public Board() {
        mBoard = new Square[ROW_SIZE][ROW_SIZE];
    }
/*
    public Square[][] getBoard() {
        return mBoard;
    }

    public void setBoard(Square[][] board) {
        mBoard = board;
    }
*/

    public Square getSquare(int column, int line) {
        return mBoard[column][line];
    }

    public void setSquare(int column, int line, Square square) {
        mBoard[column][line] = square;
    }

    public void clearBoard() {
        for(int i=0; i<9; i++) setSquare(i%ROW_SIZE, i/ROW_SIZE, Square.EMPTY);
    }

    public int getGain(int column, int line)
    {
        Square currentRole = mBoard[column][line];
        // init max gain
        int gain = 2; // vertical and horizontal lines
        if(column == line) gain++; // diagonal down
        if((column+line) == 2) gain++; // diagonal up
        for(int col=0; col<ROW_SIZE; col++) if(mBoard[col][line] != currentRole)
        { gain--; break; } //rowVertical = false;
        for(int li=0; li<ROW_SIZE; li++) if(mBoard[column][li] != currentRole)
        { gain--; break; } //rowHorizontal = false;
        if(column == line) for(int i=0; i<ROW_SIZE; i++) if(mBoard[i][i] != currentRole)
        { gain--; break; } //rowDiagonalDown = false;
        if((column+line) == 2) for(int i=0; i<ROW_SIZE; i++) if(mBoard[i][2-i] != currentRole)
        { gain--; break; } //rowDiagonalUp = false;
        return gain;
    }
}
