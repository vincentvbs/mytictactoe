package com.vincentvbs.mytictactoe.model;

import com.vincentvbs.mytictactoe.GamePresenter;

public class Game {
    private final GamePresenter mGamePresenter;
    private boolean mPlayer1_Turn;
    private Player mPlayer1;
    private Player mPlayer2;
    private Board mBoard;
    private int round;

    public Board getBoard() {
        return mBoard;
    }

    enum Step {
        INIT_GAME,
        PLAYING,
        ANNOUNCE_SCORE,
    }
    Step mStep;

    public Game(GamePresenter gamePresenter, User user1, User user2) {
        mGamePresenter = gamePresenter;
        mStep = Step.INIT_GAME;
        round = 1;
        mPlayer1 = new Player(user1);
        mPlayer2 = new Player(user2);
        mBoard = new Board();
        fsm();
    }

    public void fsm()
    {
        switch(mStep)
        {
            case INIT_GAME: // determine a role for each player, who
                boolean random_bool = true;
                mPlayer1_Turn = random_bool; // todo random call
                mBoard.clearBoard();
                if(round == 1)
                {
                    if(random_bool)
                    {
                        mPlayer1.setRole(Square.O);
                        mPlayer2.setRole(Square.X);
                        mPlayer1.setFirstname("Circle");
                        mPlayer2.setFirstname("Cross");
                        mGamePresenter.showInfo(mPlayer1.getFirstname()+" starts !");
                    }
                    else
                    {
                        mPlayer1.setRole(Square.X);
                        mPlayer2.setRole(Square.O);
                        mPlayer1.setFirstname("Cross");
                        mPlayer2.setFirstname("Circle");
                        mGamePresenter.showInfo(mPlayer2.getFirstname()+" starts !");
                    }
                }
                mStep = Step.PLAYING;
                break;
            case PLAYING:
                break;
            case ANNOUNCE_SCORE:
                break;
            //default:
                // todo log assert
        }
    }

    public void buttonPushed(int column, int line)
    {
        if(mBoard.getSquare(column, line) != Square.EMPTY) /*return;*/ mBoard.clearBoard();
        Player player, playerNextTurn;
        if(mPlayer1_Turn)
        {
            //mBoard.setSquare(column, line, mPlayer1.getRole());
            player = mPlayer1;
            playerNextTurn = mPlayer2;
        }
        else
        {
            //mBoard.setSquare(column, line, mPlayer2.getRole());
            player = mPlayer2;
            playerNextTurn = mPlayer1;
        }
        mBoard.setSquare(column, line, player.getRole());
        mPlayer1_Turn = !mPlayer1_Turn;
        mGamePresenter.refreshBoard();
        int row = mBoard.getGain(column, line);
        if(row != 0){
            player.addPoints(row);
            mGamePresenter.showInfo(mPlayer1.getFirstname()+": "+mPlayer1.getPoints()+" vs "+mPlayer2.getFirstname()+": "+mPlayer2.getPoints());
            //mGamePresenter.showInfo(player.getFirstname()+": "+row);
        }
        if(mPlayer1.getPoints()==0 && mPlayer2.getPoints()==0) mGamePresenter.showInfo(playerNextTurn.getFirstname()+" playing...");
    }
}
