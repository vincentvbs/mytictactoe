package com.vincentvbs.mytictactoe.model;

public enum Square {
        EMPTY,
        O,
        X,
}
