package com.vincentvbs.mytictactoe.model;

public class Player extends User {
    private Square mRole;
    private int mScore;
    public Player(User user1) {
        super();
        mRole = Square.EMPTY;
    }

    public void setRole(Square Q) {
        mRole = Q;
        mScore = 0;
    }

    public Square getRole() {
        return mRole;
    }

    public void addPoints(int points) {
        mScore += points;
    }

    public int getPoints() { return mScore; }
}
