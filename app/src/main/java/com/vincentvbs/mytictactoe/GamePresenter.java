package com.vincentvbs.mytictactoe;

import com.vincentvbs.mytictactoe.model.Game;
import com.vincentvbs.mytictactoe.model.Square;
import com.vincentvbs.mytictactoe.model.User;

public class GamePresenter { // is it really good
    GameView mGameView;
    Game mGame;
    User mUser1, mUser2;

    public GamePresenter(GameView gameView) {
        mGameView = gameView;
        cleanBoard();
        mUser1 = new User();
        mUser2 = new User();
        //mGame = new Game(mGameView, mUser1, mUser2); possible pattern ?
        mGame = new Game(this, mUser1, mUser2);
    }


    public void buttonPushed(int buttonIndex) {
        mGame.buttonPushed(buttonIndex%3, buttonIndex/3);
        //if(true) mGameView.placeCircle(buttonIndex);
        //else mGameView.placeCross(buttonIndex);
    }

    public void cleanBoard() {
        for(int i=0; i<9; i++) mGameView.clearSquare(i);
        //mButtons[i].setVisibility(View.INVISIBLE); hide button and no click possible
    }

    public void refreshBoard() { // todo question should I create interface for this
        for(int i=0; i<9; i++)
        {
            int column = i%3;
            int line = i/3;
            switch (mGame.getBoard().getSquare(column, line))
            {
                case O:
                    mGameView.placeCircle(i);
                    break;
                case X:
                    mGameView.placeCross(i);
                    break;
               //default:
                    // todo log assert
                // no break do following Square.EMPTY functions
                case EMPTY:
                    mGameView.clearSquare(i);
                    break;
            }
        }

    }

    public void showInfo(String text) {
        mGameView.showInfo(text);
    }
}

