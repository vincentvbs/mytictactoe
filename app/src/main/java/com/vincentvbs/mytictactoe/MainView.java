package com.vincentvbs.mytictactoe;

interface MainView {
    void showPlayButton(boolean show);

    void startGameActivity();

    void setGreetingText(CharSequence s);
}
