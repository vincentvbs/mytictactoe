package com.vincentvbs.mytictactoe;

import com.vincentvbs.mytictactoe.model.User;

public class MainPresenter {
    private MainView mMainView;
    private User mUser;

    public MainPresenter(MainView mainView) {
        mMainView = mainView;
        mUser = new User();
        mMainView.showPlayButton(false);
    }

    public void onDestroy() { mMainView = null; }


    public void playPushed() {
        mMainView.startGameActivity();
    }

    public void nameChanged(CharSequence s) {
        mMainView.showPlayButton(s.toString().length() != 0);
        mUser.setFirstname(s.toString());
        mMainView.setGreetingText("Let's go "+mUser.getFirstname()+" !");
    }
}
