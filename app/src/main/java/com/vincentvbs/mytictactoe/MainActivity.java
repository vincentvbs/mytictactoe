package com.vincentvbs.mytictactoe;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.vincentvbs.mytictactoe.R;

public class MainActivity extends AppCompatActivity implements MainView {
    private MainPresenter mMainPresenter;
    private TextView mGreetingText;
    private EditText mNameInput;
    private Button mPlayButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mGreetingText = (TextView) findViewById(R.id.activity_main_greeting_txt);
        mNameInput = (EditText) findViewById(R.id.activity_main_name_input);
        mPlayButton = (Button) findViewById(R.id.activity_main_play_btn);

        mMainPresenter = new MainPresenter(this);

        // disable name input :
        mNameInput.setVisibility(View.INVISIBLE);
        showPlayButton(true);


        findViewById(R.id.activity_main_play_btn).setOnClickListener(v -> playPushed());

        mNameInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mMainPresenter.nameChanged(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void playPushed() {
        mMainPresenter.playPushed();
    }

    @Override
    protected void onDestroy() {
        mMainPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showPlayButton(boolean show) {
        mPlayButton.setEnabled(show);
    }

    @Override
    public void startGameActivity() {
        Intent gameActivity = new Intent(MainActivity.this, GameActivity.class);
        startActivity(gameActivity);
    }

    @Override
    public void setGreetingText(CharSequence s) {
        mGreetingText.setText(s);
    }
}
