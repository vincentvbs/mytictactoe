package com.vincentvbs.mytictactoe;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class GameActivity extends AppCompatActivity implements GameView, View.OnClickListener {

    private TextView mTextInfo;
    private ImageButton mButtons[];
    private GamePresenter mGamePresenter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        mTextInfo = (TextView) findViewById(R.id.activity_game_text_info);
        mButtons = new ImageButton[9];
        mButtons[0] = (ImageButton) findViewById(R.id.activity_game_button_0);
        mButtons[1] = (ImageButton) findViewById(R.id.activity_game_button_1);
        mButtons[2] = (ImageButton) findViewById(R.id.activity_game_button_2);
        mButtons[3] = (ImageButton) findViewById(R.id.activity_game_button_3);
        mButtons[4] = (ImageButton) findViewById(R.id.activity_game_button_4);
        mButtons[5] = (ImageButton) findViewById(R.id.activity_game_button_5);
        mButtons[6] = (ImageButton) findViewById(R.id.activity_game_button_6);
        mButtons[7] = (ImageButton) findViewById(R.id.activity_game_button_7);
        mButtons[8] = (ImageButton) findViewById(R.id.activity_game_button_8);
        mGamePresenter = new GamePresenter(this);


        // Use the same listener for the four buttons.
        // The tag value will be used to distinguish the button triggered
        for(int i=0; i<9; i++) mButtons[i].setOnClickListener(this);

        // Use the tag property to 'name' the buttons
        for(int i=0; i<9; i++) mButtons[i].setTag(i);
    }


    @Override
    public void onClick(View v) {
        int buttonIndex = (int) v.getTag();
        mGamePresenter.buttonPushed(buttonIndex);
    }

    public void showInfo(String s)
    {
        //Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        mTextInfo.setText(s);
    }

    private void placeImage(int buttonIndex, int resId) {
        mButtons[buttonIndex].setImageResource(resId);
    }

    public void placeCircle(int buttonIndex)
    {
        //showInfo("place circle at position "+buttonIndex);
        placeImage(buttonIndex, R.drawable.circle);
    }

    public void placeCross(int buttonIndex)
    {
        //showInfo("place cross at position "+buttonIndex);
        placeImage(buttonIndex, R.drawable.cross);
    }

    public void clearSquare(int buttonIndex)
    {
        //showInfo("clear square at position "+buttonIndex);
        placeImage(buttonIndex, android.R.color.transparent);
    }
}
